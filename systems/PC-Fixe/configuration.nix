# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ../common-cli.nix
      ../common-gui.nix
    ];

  nix.settings.trusted-users = [ "root" "paul" ];

  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub = {
    efiSupport = true;
    device = "nodev";
    zfsSupport = true;
    memtest86.enable = true;
    fontSize = 32;
  };
  boot.kernelParams = [
    "acpi_enforce_resources=lax"
    "zfs.zfs_arc_max=2147483648"
  ];
  boot.tmp.useTmpfs = false;
  boot.supportedFilesystems = [ "zfs" ];
  boot.extraModprobeConfig = ''
    options hid_apple fnmode=2
  '';

  zramSwap.enable = true;

  virtualisation.podman.enable = true;

  services.zfs = {
    trim = {
      enable = false;
      interval = "monthly";
    };
    autoScrub = {
      enable = false;
      interval = "monthly";
    };
    autoSnapshot = {
      enable = true;
      monthly = 6;
    };
    autoReplication = {
      enable = true;
      host = "nyanlout.re";
      username = "zfspaulfixe";
      identityFilePath = "/var/lib/zfs-replication/id_rsa";
      localFilesystem = "fastaf/home";
      remoteFilesystem = "loutrepool/zfs-replicate/paul-fixe";
    };
  };

  hardware.bluetooth.enable = true;

  # Logitech G920
  hardware.usb-modeswitch.enable = true;

  services.udev.packages = with pkgs; [
    usb-modeswitch-data # Logitech G920
  ];

  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="0664", GROUP="dialout"
  '';

  networking.hostName = "paul-fixe";
  networking.hostId = "3a1f739e";

  networking.hosts = {
    "10.30.0.1" = ["emby.nyanlout.re" "nyanlout.re"];
  };

  environment.systemPackages = with pkgs; [
    usb-modeswitch
    esphome
  ];

  programs = {
    wireshark.enable = true;
    alvr.enable = true;
  };

  networking.firewall.enable = false;

  services.displayManager.autoLogin.user = "paul";

  users.users.paul = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "wireshark" "input" "dialout" "libvirtd" "vboxusers" ];
    uid = 1000;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDstFRwMoTEip5IBSYE4dUj3miO0LsKrnUKQJmp7d5QYo3VhXk43jU6VUU0tVAegkzWLlQ3ohoFns+8bZyf7hj7roftrDfoC9bbbx4ihhWrZTlF0gzoH4t52yetFO5eC/tV2sm/zFoa+3IWLokOEFmAoknAVag1MmVLXTQ6WPoTPD4UsX/D3lyE4dbSKxHpMOIjqIdqSEgO0BeTdnHe5afvGXXO1VYTvPsGDHT9w8EHwQV9JXIPn7KVOp3qin7OwvFFrrB3QbiEVTJvGiH2hrfxcARTN/+TxGtf+aOFeuQykURG9Wz/aBK60EWE0wGrzuIymxtNdOR1NhmnNrUZ976Tb9WdR7FC+yM6+/kdfICy+sGQmmn8TLsGvcJTT/pl4Pa9uRAKjRJuLIEgYY6W/ms9lCRyf484yRkDlq+V0BPuN9Jy6Eb7x+tmZNkpEtkqso7wfXD8sf5BIwv2K69SVMpfTswydHGmDwHZ0zaDKGlyCiyJ1QGqUhCTXqtYVq+kQ3AcjKcysMwVEmwx/ySu0XFuV8oUkl9XK/RUoc++sMEd0EbHcn8uwCmBARNX+GLQ03vxwyMW3HyneP8EAxoqtSepZXbTdVP/0i+l7EUUeA7zsaWfU2a82ktZWpVPFGfxkuo0l3zLF19EsXPKZNqlRfkOWjSgp+qWihAkQIQk3GoduQ== openpgp:0x75EE3375"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3fEmkmrhccW8NegIk/Ubu6Yw80VCQ1ttG419e+1V1wkJPXFAqcIhffwrIlz81dJ47T+H+zeptpAX8U1Gbk1B5ZH4DW8OcqU6ymM+j6g/gICpvrjJUOpdgyA3GIOjuBJGijGQGggDw1k2SdopAVV1H38YUAJ33RGDvjLJO6VREYLDYLF4oaDp8ann7Wn8BpX2T7cRvhrzqcwbEGaw1f/xrLE5KklOb6pOHRWFJMxW83d8OKiLkQvM4vFGlvvG0/AKGZaZWHDXS7ldoyAv+vnN8DrIxmWEQjdNLfAwYDBHp6XqE0slde4dqBjVHji5+ajFr7eJnrzc4IXsHJ1jM9xGB paul@loutreos"
    ];
  };

  services.openssh.enable = true;
  services.openssh.settings = {
    PasswordAuthentication = false;
    X11Forwarding = true;
  };

  services.xserver.deviceSection = ''
    Option "metamodes" "DP-4: 3440x1440_144 +0+0 {AllowGSYNCCompatible=On}"
  '';

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];

  systemd.services = {
    zfs-replication.serviceConfig.StateDirectory = "zfs-replication";
  };

  boot.enableContainers = false;
  system.stateVersion = "20.03";
}
