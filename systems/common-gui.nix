{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    filezilla
    qbittorrent
    transmission-remote-gtk

    sc-controller
    steam-run
    prismlauncher
    lutris
    teamspeak_client
    ryujinx

    betaflight-configurator

    ledger-live-desktop
    monero-gui

    tor-browser-bundle-bin
    brave

    tdesktop
    element-desktop
    mumble
    discord

    kleopatra
    gnupg
    gopass
    xclip

    kdeplasma-addons
    ark
    kate
    kmail
    kdePackages.kdeconnect-kde
    okular
    yakuake
    konversation
    gwenview
    kcalc
    spectacle
    kinfocenter
    kile
    (texlive.combine {
      inherit (texlive) scheme-small titling collection-langfrench cm-super;
    })

    libsForQt5.breeze-gtk

    libreoffice

    gimp
    inkscape
    imagemagick
    obs-studio
    vlc
    mpv
    kdenlive

    glxinfo
    i7z
    pavucontrol
  ];

  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  console.keyMap = "fr";

  networking.networkmanager.enable = true;

  systemd.extraConfig = "DefaultLimitNOFILE=1048576";

  security = {
    pam.loginLimits = [{
      domain = "*";
      type = "hard";
      item = "nofile";
      value = "1048576";
    }];
    rtkit.enable = true;
  };

  programs = {
    gnupg.agent = { enable = true; enableSSHSupport = true; };
    browserpass.enable = true;
    steam.enable = true;
    firefox.enable = true;
    appimage.enable = true;
  };

  services = {
    # desktopManager.plasma6.enable = true;
    displayManager = {
      sddm = {
        enable = true;
        # wayland.enable = true;
        autoLogin.relogin = true;
      };
    };
    xserver = {
      enable = true;
      xkb.layout = "fr";
      exportConfiguration = true;
      desktopManager.plasma5.enable = true;
    };
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      extraConfig.pipewire = {
      "10-clock-rate" = {
        "context.properties" = {
          "default.clock.allowed-rates"  = [ 48000 ];
        };
      };
    };
    };
    udev.packages = with pkgs; [ ledger-udev-rules ];
    pcscd.enable = true;
  };

  environment.etc = {
    "mpv/mpv.conf" = {
      text = ''
        profile=gpu-hq
        scale=ewa_lanczossharp
        cscale=ewa_lanczossharp
        video-sync=display-resample
        interpolation
        tscale=oversample
      '';
    };
    # CK3 fix
    "ssl/certs/f387163d.0".source = "${pkgs.cacert.unbundled}/etc/ssl/certs/Starfield_Class_2_CA.crt";
  };
}
