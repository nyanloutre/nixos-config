{ lib, config, pkgs, ... }:

{
  users.users = {
    paul = {
      uid = 1000;
      isNormalUser = true;
      description = "Paul TREHIOU";
      extraGroups = [ "wheel" "medias" "transmission" "podman" ];
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAF7VlzHzgg70uFbRFtVTS34qNBke/RD36mRENAsa33RxztxrqMsIDscAD/d6CTe6HDy7MCGzJnWCJSXj5iOQFM4RRMvKNEgCKPHqfhmfVvO4YZuMjNB0ufVf6zhJL4Hy43STf7NIWrenGemUP+OvVSwN/ujgl2KKw4KJZt25/h/7JjlCgsZm4lWg4xcjoiKL701W2fbEoU73XKdbRTgTvKoeK1CGxdAPFefFDFcv/mtJ7d+wIxw9xODcLcA66Bu94WGMdpyEAJc4nF8IOy4pW8AzllDi0qNEZGCQ5+94upnLz0knG1ue9qU2ScAkW1/5rIJTHCVtBnmbLNSAOBAstaGQJuSL40TWZ1oPA5i1qUEhunNcJ+Sgtp6XP69qY34T/AeJvHRyw5M5LfN0g+4ka9k06NPBhbpHFASz4M8nabQ0iM63++xcapnw/8gk+EPhYVKW86SsyTa9ur+tt6oDWEKNaOhgscX44LexY7jKdeBRt3GaObtBJtVLBRx3Z2aRXgjgnKGqS40mGRiSkqb2DShspI1l8DV2RrPiuwdBzXVQjWRc0KXmJrcgXX9uoPSxihxwaUQyvmITOV1Y+NEuek4gRkVNOxjoG7RGnaYvYzxEQVoI5TwZC2/DCrAUgCv8DQawkcpEiWnBq7Q5VnpmFx5juVQ/I0G8byOkPXgRUOk9 openpgp:0xAB524BBC"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCACVI2dL4AmOdcb7RSl3JZpfK33NhqrYFfWfXMYow5SPJ9VPteOp5kVvKUuSbGH3chjpttYC/ueQJJhFagiKmkeftQTslIw6C009wUExGpJwRotHqISXv2ctURGURKy2FF848whd7xZJzdj49ZJ6S+SCbRFZvVWfT2dP/JwTiWW1mbEaWKyOgrixH6wSKt9ECumjX9KjqSWGw+k3yLJxIhdqiZAjNv4soJs1mQYdIlFCXCuznzoZIQBexZPQCx0j9HjczPz1feMLWkrHzEMexNjsBE2uA6yXLbH1wa8xCJ4VOeD7u9JqVY579AsicD62G+qIgw0B2zmyz7xTrdPv+061zmYn6qYr8EXGTk4dVgedZp8M1XzZ1PVoeeftPFcClXC7zCGyCR2uzJbQLzlaTwZrdghAiS9UhMRuKpNgZy2zDWw4MqdojrF5bndPkoijlXWYrPYBFED5OU1mpwzpanYgldowJC/Ixjwi+Hmse2q4XgZ+egfuotBqPfqB+bWsCa5GNiJWGdLP69uBSsXubGnqLwvE0FAQ2GHb+SEoZKFy/QV9GzOLlVrGlgK5YFgKJD+Q1nn1QRycXt1oMVC/AtR/NshOGanhdvIRpPATGmaxLVXSY093vyAOW4MPrS00fPAXzAfJUwIuWcloFfLMo5Jitj5rpE1s6FX8xrl4upQ== paul@nyanlout.re"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDstFRwMoTEip5IBSYE4dUj3miO0LsKrnUKQJmp7d5QYo3VhXk43jU6VUU0tVAegkzWLlQ3ohoFns+8bZyf7hj7roftrDfoC9bbbx4ihhWrZTlF0gzoH4t52yetFO5eC/tV2sm/zFoa+3IWLokOEFmAoknAVag1MmVLXTQ6WPoTPD4UsX/D3lyE4dbSKxHpMOIjqIdqSEgO0BeTdnHe5afvGXXO1VYTvPsGDHT9w8EHwQV9JXIPn7KVOp3qin7OwvFFrrB3QbiEVTJvGiH2hrfxcARTN/+TxGtf+aOFeuQykURG9Wz/aBK60EWE0wGrzuIymxtNdOR1NhmnNrUZ976Tb9WdR7FC+yM6+/kdfICy+sGQmmn8TLsGvcJTT/pl4Pa9uRAKjRJuLIEgYY6W/ms9lCRyf484yRkDlq+V0BPuN9Jy6Eb7x+tmZNkpEtkqso7wfXD8sf5BIwv2K69SVMpfTswydHGmDwHZ0zaDKGlyCiyJ1QGqUhCTXqtYVq+kQ3AcjKcysMwVEmwx/ySu0XFuV8oUkl9XK/RUoc++sMEd0EbHcn8uwCmBARNX+GLQ03vxwyMW3HyneP8EAxoqtSepZXbTdVP/0i+l7EUUeA7zsaWfU2a82ktZWpVPFGfxkuo0l3zLF19EsXPKZNqlRfkOWjSgp+qWihAkQIQk3GoduQ== openpgp:0x75EE3375"
      ];
    };
  
    victor = {
      uid = 1111;
      isNormalUser = true;
      description = "Victor SENE";
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCaCc2qcQsd/VPDX1K97zJ/MjObWnTfRkA98xeNnVTeGoMmbf/fW1KszB+3IYCngCKLhliotHEXkqOK24vMLZ9ylVPTIPLNY5OWLFRQSOU/OykP8r4ikDWmMOwI+tqkoBknTUZdA1MN1JmkpE1cWL8vRJ7mnwl/p7xCMHV19N5+UdoIx3bFsm0CCUMIYSaIefoD3tpIzmgVqBkIH4FSmNwvXHwcXhhOTGqXbTJYC/GY2mI0AFvZI3T61ReV325ms7QRQlElXP8Rv8lpjr57VISNjKsPPNvifLjy10RjIS8iNioN6fJ0XBCfKOmm37VX86aFkAgWdpzxGojprOXnViwZEWSegnvKGx8FNx3gB54zF76e6koWS+qnYf9UTdogO6uXhXZb7AoMC9XD0/l6Egh8HzPWAUZtLx74zhB8ufoKmzqOp4YrCK8Cu4N/1UTFyPUoeSCZJdcE/9iqldym06mOi4rDV5cKCzs+Q0bVP8+x8SZ9ajYUH7l4sxjDHtHiyAYniWIxGPO5NazCfx7J+1GGzbtV0HuBwE/U20z+nBy+WZ4MowQTNb2E2uhq4OgQASx7uTKtyhnfT09A3toHZVerfH8ET8YEVnwdYMCA0GHYMOZ48h1ORdE+OLyqRlxjYTCB17Kc2icSL3iv8Yd66vQagy3A+C8OhZP1rdc15bEZnw== openpgp:0x28DF0235"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1r2ZzVnOlmoNoLgrc3+Lx7whO8mzcwUf2p9DiYAVg2zo2zbfubLVG1BAgFDe7y+2HwJIbGDDMNUaT+FAsv0mHRlfdUMXXF3nVsFPWGovo1ks31O5zUI9IE3qFU5AJ7SPICS4lQYox1o594iS1OcwJ7Iu6pjEQRRG1OLVYSILJ994vtGsDxfz1CZ8b7u9oSwHz0E4pdy6epkFSE/+9WsZl+ziDMigYZfubjzUCzMy2uT5Z6t+r6bW6mcxnmYax/YmrRvL/dTeDE64Qf7nugjB0XOKUOKCPN5dtqYRx0fN9aDSRf4ubmyVaYeKudm9vttGHXjSPVWAvow+jUDOq2cGr victor@sene.ovh"
      ];
    };

    amandoleen = {
      isNormalUser = true;
      isSystemUser = lib.mkForce false;
      description = "Amandine <3";
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRu0M10AfF2nS8Ky9JzQ4cDRbcjmR+LLvENohxTJhW4xwAwN/f2I0ObbiJ5PHC3mXi8V5JqnSaYNdIMXuULDxxc1QEa0aeLw4xIrYw1wEOrHvL9DeYvS289sRgWSb/PvyejEHwcBBm4Xf0/nQNQF462C064Ms56cT3YAOhNBA7ubyCAqd0VW8RHc1mZ5b/owzM7SviXrpcIM5QDPVP2051SxrqGbunAnwe5x2kNDeYqicyIik61rpYe+erhkyvL3WdgC7mHnF8VPKx049zIsfK5UmJwI+rI24cDUT9E6B4XZttpdyJK6i7/6NSYm4nrMXQxCF1tofd5yZK5vpYH8Dyiic5ykhB57kr1QCmck70VMmt77uOoMeGZ+IJCY5oZYcR/n04mUwN9mmRsECcr17a8IDQkmPd8PNbjrNRgtVKDVLtw4HyOPCAfdH+XzEGiMxnZfzq0rLEYU3S4RAkLpe2W3C0fWniEWzSnXj76AGcyJcxIVzrIYg4uzTgNhZlb08vO1ytw9QaEY17Po5YFjBD8fT9Z0UijG3WQBm7AmSrcg/OLP6UOghbnlkZFYpC0hYcbX1sTfdfysw9kT/d8BImQvwBzmJ/YRqHmCejZGTkCCmw8i93EYFm3uVFZypcSe50JV9rx9P7yPtWS4QVOg/6ltlvJiGsz45P87qjA9nO/Q== amandoleen"
      ];
    };

    synology = {
      uid = 1001;
      isNormalUser = true;
      isSystemUser = lib.mkForce false;
      description = "Synology Diskstation maison";
    };

    rezome = {
      description = "Rezome Minecraft backup";
    };

    zfspaulfixe = {
      uid = 1002;
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIQjzpEBNEwJhBTh82K5bJMyPSuHvYJyipaZYVtOZSJIDIRcec9YwMOQuMl9+O5kI36tiataEpmb/y3OWU567DXKp/BtljfVfNSQracKJPt6GJFOz4fKqCqoBh7M06ZGsBHUi/NsKIvw4asJsBuK6uACvxKFt2pSR6fp+/2y/3NH0LpDmDuuYORhciJUBC+RUTCLtVUfzkMAeR2p+M079Ia0rnkEXNFgc75FQon1bQu/0bnbHPweSwnNfRB0XPm6Qgz3sS2Cyhf1/GqgvARs80P4MWuqqZFXhsBTajOHOjCu6fiaylPNXNWfLnahkRzKIOkIy7/yEO+MmMD+V7pjbvSqB7MD1T5wfcPHvHTISM4E+7dw9aPj9+JspkgSSACtIFKsqxkWlq5creUV1syDm2Cd4rBnKrc8yWtAxBSusFWbAr6xtxP6I+ibS4trjNdHHrjMJK9jpbPxdblJJrO3qn6VYC/WzFAxzDDUi5ysGzNdpQLacacne/95gSGXCgIIc= root@paul-fixe"
      ];
      description = "paul-fixe zfs backup user";
    };
  };

  users.extraGroups.medias = {
    gid = 498;
    members = [ "slimserver" "radarr" "sonarr" "jellyfin" "transmission" ];
  };
}
