{ config, pkgs, inputs, ... }:

{
  boot = {
    kernel.sysctl = {
      "net.ipv6.conf.all.forwarding" = true;
      "net.ipv6.conf.default.forwarding" = true;
      "net.ipv4.conf.all.forwarding" = true;
      "net.ipv4.conf.default.forwarding" = true;
    };
  };

  # Enable LTE drivers
  hardware.usb-modeswitch.enable = true;

  ##################
  # NETWORK CONFIG #
  ##################

  # eno1      -> VLAN100 -> Internet
  # eno2      -> LAN
  # eno3      -> Pas utilisé
  # eno4      -> Pas utilisé
  # enp0s21u1 -> Clé 4G Bouygues
  # wg0       -> Tunnel Wireguard ARN

  networking = {
    hostName = "loutreos"; # Define your hostname.
    hostId = "7e66e347";

    useNetworkd = true;
    useDHCP = false;

    nameservers = [
      # https://www.dns0.eu/fr
      "193.110.81.0"
      "185.253.5.0"
    ];

    vlans = {
      bouygues = {
        id = 100;
        interface = "eno1";
      };
    };

    interfaces = {
      bouygues = {
        # Adresse MAC BBox : https://lafibre.info/remplacer-bbox/informations-de-connexion-ftth/msg598303/#msg598303
        macAddress = "E8:AD:A6:21:73:68";
        useDHCP = true;
      };
      eno2 = {
        ipv4.addresses = [
          { address = "10.30.0.1"; prefixLength = 16; }
        ];
      };
      enp0s21u1.useDHCP = true;
    };

    nftables = {
      enable = true;
      flushRuleset = false;
      tables = {
        "multi-wan-routing" = {
          family = "inet";
          content = ''
            chain PREROUTING {
              type filter hook prerouting priority mangle; policy accept;
              # Restore the packet's CONNMARK to the MARK for existing incoming connections
              counter meta mark set ct mark
              # If packet MARK is set, then it means that there is already a connection mark
              meta mark != 0x00000000 counter accept
              # Else, we need to mark the packet.
              # If the packet is incoming on bouygues then set MARK to 1, LTE MARK 2 and VPN MARK 3
              iifname "bouygues" counter meta mark set 0x1
              iifname "enp0s21u1" counter meta mark set 0x2
              iifname "wg0" counter meta mark set 0x3
              # Save new mark in CONNMARK
              counter ct mark set mark
            }

            chain OUTPUT {
              type route hook output priority mangle; policy accept;
              # Restore CONNMARK to MARK for outgoing packets before final routing decision
              counter meta mark set ct mark
            }

            chain POSTROUTING {
              type filter hook postrouting priority mangle; policy accept;
              # Save MARK to CONNMARK
              counter ct mark set mark
            }
          '';
        };

        "redirect-external-to-local" = {
          family = "ip";
          content = ''
            chain PREROUTING {
              type nat hook prerouting priority dstnat; policy accept;
              # Redirect local network request from server external IP to internal IP
              # This allow access to server without internet access
              ip saddr 10.30.0.0/16 ip daddr 176.180.172.105 counter dnat to 10.30.0.1
            }
          '';
        };
      };
    };

    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 ];
      allowedUDPPorts = [ ];

      # Open ports on local netwok only
      interfaces.eno2 = {
        allowedTCPPorts = [
          111 2049 4000 4001 4002 # NFS
          3483 9000 9090 # Slimserver
          1935 # RTMP
        ];
        allowedUDPPorts = [
          111 2049 4000 4001 4002 # NFS
          3483 # Slimserver
          67 # DHCP
        ];
      };

      # Don't forward incoming IPv6 requests to local network
      filterForward = true;
      extraForwardRules = ''
        # Forward all IPv6 traffic from local network
        iifname "eno2" counter accept
      '';
    };
  };

  systemd.services.systemd-networkd = {
    unitConfig = {
      RequiresMountsFor = "/mnt/secrets/wireguard";
    };
    serviceConfig = {
      LoadCredential = [
          "network.wireguard.private.wg0:/mnt/secrets/wireguard/wireguard.private"
          "network.wireguard.preshared.wg0:/mnt/secrets/wireguard/wireguard.preshared"
      ];
    };
  };

  #################
  # ROUTING RULES #
  #################

  # 0:      from all lookup local
  # 60:     from all iif lo dport 25 lookup vpn  # mails are forced to vpn table
  # 32766:  from all lookup main                 # main table should contain no default routes, only local network routes
  # 32767:  from all lookup default
  # 41000:  from all fwmark 0x1 lookup fiber     # fwmark indicate established connection that must go through same interface
  # 42000:  from all fwmark 0x2 lookup lte
  # 43000:  from all fwmark 0x3 lookup vpn
  # 51000:  from all lookup fiber                # first table encountered with a default route if fiber is up
  # 52000:  from all lookup lte                  # first table encountered with a default route if fiber is down

  systemd.network = let
    routeTables = {
      fiber = 1;
      lte = 2; 
      vpn = 3;
    };
  in {
    enable = true;

    config = {
      inherit routeTables;
      addRouteTablesToIPRoute2 = true;
    };

    # Wireguard ARN device configuation
    netdevs = {
      "10-wg0" = {
        netdevConfig = {
          Kind = "wireguard";
          Name = "wg0";
          MTUBytes = "1450";
        };
        wireguardConfig = {
          PrivateKey = "@network.wireguard.private.wg0";
          RouteTable = routeTables.vpn;
        };
        wireguardPeers = [
          {
            Endpoint = "89.234.141.83:8095";
            PublicKey = "t3+JkBfXI1uw8fa9P6JfxXJfTPm9cOHcgIN215UHg2g=";
            PresharedKey = "@network.wireguard.preshared.wg0";
            AllowedIPs = ["0.0.0.0/0" "::/0"];
            PersistentKeepalive = 15;
          }
        ];
      };
    };

    networks = {
      #########
      # FIBER #
      #########

      # Set route metric to highest priority
      # Set DHCP client magic settings for Bouygues
      "40-bouygues" = {
        dhcpV4Config.RouteTable = routeTables.fiber;

        dhcpV6Config = {
          DUIDRawData = "00:03:00:01:E8:AD:A6:21:73:68";
          WithoutRA = "solicit";
        };

        ipv6AcceptRAConfig = {
          DHCPv6Client = true;
          RouteTable = routeTables.fiber;
        };

        networkConfig = {
          KeepConfiguration = "dhcp-on-stop";
          IPv6AcceptRA = true;
          DHCPPrefixDelegation = true;
        };

        # Static attribution of first IPv6 subnet
        dhcpPrefixDelegationConfig.SubnetId = "0";

        # Route everything to fiber link with a priority of 40000
        routingPolicyRules = [
          {
            FirewallMark = 1;
            Table = routeTables.fiber;
            Priority = 41000;
            Family = "both";
          }
          {
            Table = routeTables.fiber;
            Priority = 51000;
            Family = "both";
          }
        ];
      };

      # Don't check VLAN physical interface as it is not directly used
      "40-eno1".linkConfig.RequiredForOnline = "no";

      #######
      # LTE #
      #######

      # Set LTE route to lower priority
      "40-enp0s21u1" = {
        dhcpV4Config.RouteTable = routeTables.lte;

        # Route all to lte link with a priority of 50000
        routingPolicyRules = [
          {
            FirewallMark = 2;
            Table = routeTables.lte;
            Priority = 42000;
            Family = "both";
          }
          {
            Table = routeTables.lte;
            Priority = 52000;
            Family = "both";
          }
        ];
      };

      #######
      # VPN #
      #######

      # Wireguard ARN network configuation
      "10-wg0" = {
        matchConfig.Name = "wg0";
        address = [
          "89.234.141.196/32"
          "2a00:5881:8119:400::1/128"
        ];
        routingPolicyRules = [
          # Route outgoing emails to VPN table
          {
            IncomingInterface = "lo";
            DestinationPort = "25";
            Table = routeTables.vpn;
            Priority = 60;
            Family = "both";
          }
          # Route packets originating from wg0 device to VPN table
          # Allow server to respond on the wg0 interface requests
          {
            FirewallMark = 3;
            Table = routeTables.vpn;
            Priority = 43000;
            Family = "both";
          }
        ];
      };

      #######
      # LAN #
      #######

      # LAN DHCP server config
      "40-eno2" = {
        networkConfig = {
          IPv6SendRA = true;
          DHCPPrefixDelegation = true;
          DHCPServer = true;
          IPMasquerade = "ipv4";
        };
        dhcpServerConfig = {
          EmitRouter = true;
          EmitDNS = true;
          DNS = [
            # https://www.dns0.eu/fr
            "193.110.81.0"
            "185.253.5.0"
          ];
        };
        dhcpServerStaticLeases = [
          # IPMI
          {
            Address = "10.30.1.1";
            MACAddress = "ac:1f:6b:4b:01:15";
          }
          # paul-fixe
          {
            Address = "10.30.50.1";
            MACAddress = "b4:2e:99:ed:24:26";
          }
          # salonled
          {
            Address = "10.30.40.1";
            MACAddress = "e0:98:06:85:e9:ce";
          }
          # miroir-bleu
          {
            Address = "10.30.40.2";
            MACAddress = "e0:98:06:86:38:fc";
          }
          # miroir-orange
          {
            Address = "10.30.40.3";
            MACAddress = "50:02:91:78:be:be";
          }
        ];
        ipv6SendRAConfig = {
          EmitDNS = true;
          DNS = [
            # https://www.dns0.eu/fr
            "2a0f:fc80::"
            "2a0f:fc81::"
          ];
        };
      };
    };
  };
}
