{ config, lib, pkgs, pkgs-4a3fc4cf7, ... }:

{
  services = {
    transmission = {
      enable = true;
      package = pkgs-4a3fc4cf7.transmission_4;
      home = "/var/lib/transmission";
      group = "medias";
      settings = {
        rpc-bind-address = "127.0.0.1";
        rpc-port = 9091;
        rpc-host-whitelist = "*";
        rpc-whitelist-enabled = false;
        peer-port = 51413;
        incomplete-dir = "/mnt/medias/incomplete";
        download-dir = "/mnt/medias/torrent";
      };
    };

    radarr.enable = true;
    sonarr.enable = true;
    prowlarr.enable = true;
    jellyfin.enable = true;
    slimserver.enable = true;
  };

  systemd.services.transmission.serviceConfig = {
    BindPaths = [ "/mnt/medias" ];
    LimitNOFILE = 1048576;
  };

  networking = {
    firewall.allowedTCPPorts = [
      config.services.transmission.settings.peer-port
    ];

    firewall.allowedUDPPorts = [
      config.services.transmission.settings.peer-port
    ];
  };
}
