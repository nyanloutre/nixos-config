{ config, lib, pkgs, ... }:

with lib;

let
  domaine = "nyanlout.re";

  sendMail = to: subject: message: pkgs.writeShellScriptBin "mail.sh" ''
    ${pkgs.system-sendmail}/bin/sendmail ${to} <<EOF
    From: root@nyanlout.re
    Subject: ${subject}
    ${message}
    EOF
  '';

  login_mail_alert = pkgs.writeShellScriptBin "mail_alert.sh" ''
    if [ "$PAM_TYPE" != "close_session" ] && [ "$PAM_USER" != "zfspaulfixe" ] && [ "$PAM_USER" != "synology" ] && [ "$PAM_USER" != "rezome" ]; then
      ${sendMail "paul@nyanlout.re" "SSH Login: $PAM_USER from $PAM_RHOST" "`env`"}/bin/mail.sh
    fi
  '';

  backup_mail_alert = sendMail "paul@nyanlout.re" "ERREUR: Sauvegarde Borg" "Impossible de terminer la sauvegarde. Merci de voir les logs";
in

{
  imports = [
    ./monitoring.nix
    ./medias.nix
    ./web.nix
  ];

  security.acme.certs = {
    "${domaine}" = {
      extraDomainNames = [
        "mail.${domaine}"
      ];
      postRun = ''
        systemctl reload dovecot2.service
      '';
    };
  };

  mailserver = {
    enable = true;
    fqdn = "mail.${domaine}";
    domains = [ domaine ];

    # A list of all login accounts. To create the password hashes, use
    # mkpasswd -m sha-512 "super secret password"
    loginAccounts = {
      "paul@${domaine}" = {
        hashedPassword = "$6$eGmy2W7kbkfHAh$/y.ZML4eYL/v14WaVwSIG2ulkUFKFk82uBmrYBDULLtqUR8hQD3/BQIrRiBtsloxrUSja8aZ.E7ypChO.OiOI/";
      };
      "claire@${domaine}" = {
        hashedPassword = "$6$Y.vlWP9./DX$NEQQOLzYftbHOvXDkKdBYFAjzIjh8mlpomDuQRq6qkkZijrdy/p6jSbrpBLhoWwVmj4j1OWekHU1f4C9xCNJk.";
      };
    };

    # Certificate setup
    certificateScheme = "manual";
    certificateFile = "/var/lib/acme/${domaine}/fullchain.pem";
    keyFile = "/var/lib/acme/${domaine}/key.pem";

    # Enable IMAP and POP3
    enableImap = true;
    enablePop3 = true;
    enableImapSsl = true;
    enablePop3Ssl = true;

    # Enable the ManageSieve protocol
    enableManageSieve = true;
  };

  services = {
    rspamd.workers.controller.extraConfig = ''
      secure_ip = ["0.0.0.0/0", "::"];
    '';

    # redis.enable = true;

    # enable with nginx defult config
    logrotate.enable = true;

    fail2ban.enable = true;

    fstrim.enable = true;

    nfs.server = {
      enable = true;
      exports = ''
        /mnt/medias  10.30.0.0/16(ro,no_root_squash)
        /var/lib/minecraft  10.30.0.0/16(rw,no_root_squash)
      '';
      statdPort = 4000;
      lockdPort = 4001;
      mountdPort = 4002;
    };

    borgbackup.jobs = {
      loutre = {
        paths = [
          "/var/certs"
          "/var/dkim"
          "/var/lib/jellyfin"
          "/var/lib/gitea"
          "/var/lib/grafana"
          "/var/lib/jackett"
          "/mnt/borgsnap/postgresql"
          "/var/lib/radarr"
          "/var/lib/sonarr"
          "/var/lib/transmission"
          "/var/lib/airsonic"
          "/var/lib/hass"
          "/var/lib/opendkim"
          "/var/lib/slimserver"
          "/var/lib/watcharr"
          "/var/lib/nextcloud"
          "/mnt/paul-home/paul"
          "/var/sieve"
          "/var/vmail"
          "/mnt/backup_loutre/amandoleen"
          "/mnt/secrets"
        ];
        exclude = [
          "/var/lib/radarr/.config/Radarr/radarr.db-wal"
          "/var/lib/radarr/.config/Radarr/radarr.db-shm"
          "/mnt/paul-home/paul/.cache"
        ];
        repo = "ssh://u306925@u306925.your-storagebox.de:23/./loutreos";
        environment = { BORG_RSH = "ssh -i /mnt/secrets/hetzner_ssh_key"; };
        encryption = {
          mode = "repokey-blake2";
          passCommand = "cat /mnt/secrets/borgbackup_loutre_encryption_pass";
        };
        startAt = "weekly";
        prune.keep = {
          within = "1d";
          weekly = 4;
          monthly = 12;
        };
        preHook = ''
          ${pkgs.zfs}/bin/zfs snapshot loutrepool/var/postgresql@borgsnap
          mkdir -p /mnt/borgsnap/postgresql
          ${config.security.wrapperDir}/mount -t zfs loutrepool/var/postgresql@borgsnap /mnt/borgsnap/postgresql
        '';
        readWritePaths = [ "/var/lib/postfix/queue/maildrop" ];
        postHook = ''
          ${config.security.wrapperDir}/umount /mnt/borgsnap/postgresql
          ${pkgs.zfs}/bin/zfs destroy loutrepool/var/postgresql@borgsnap
        '';
      };
    };

    borgbackup.repos = {
      diskstation = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDllbxON66dBju7sMnhX8/E0VRo3+PDYvDsHP0/FK+h8JHol4+pouLmI7KIDKYOJmSuom283OqnyZOMqk+RShTwWIFm9hOd2R9aj45Zrd9jPW2APOCec/Epgogj0bwBnc0l2v6qxkxaBMgL5DnAQ+E00uvL1UQpK8c8j4GGiPlkWJD6Kf+pxmnfH1TIm+J2XCwl0oeCkSK/Frd8eM+wCraMSzoaGiEcfMz2jK8hxDWjDxX7epU0ELF22BVCuyN8cYRoFTnV88E38PlaqsOqD5ePkxk425gDh7j/C06f8QKgnasVH2diixo92kYSd7i/RmfeXDDwAD5xqUvODczEuIdt root@DiskStation" ];
        path = "/mnt/backup_loutre/diskstation_borg";
        user = "synology";
      };
      minecraft-rezome = {
        authorizedKeys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDc1nGsSesW96k0DPMSt/chjvCrYmfgPgHG1hdUYB5x0pZPdOJaVRIlETWdoFlO+ViviC518B3TF7Qc3oJXPZMchJQl684Nukbc312juf+j9z/KT3dqD8YvKX6o5ynx1Dyq52ftrfkBAEAvzE0OfRljUPbwGBOM0dGRD4R1jbiHquTXpITlbgGTZymbwr4Jr9W9atgf5kHMiX7xOqMZcasDtUE8g+AG4ysHdpjOrBOUM9QeRbVP1bxEFP8xjqOOoET5tbkwektP4B2jaf+EHBPUy2lkwjVEKT6MaSlkJx/wMvUWp25kG9mrXgwUw1bgfOeZIsK6ztcki3l92BJQD9ip shame@minecraft.rezom.eu" ];
        path = "/mnt/backup_loutre/minecraft_rezome";
        user = "rezome";
      };
    };

    kresd = {
      enable = true;
    };

    mosquitto = {
      enable = true;
      listeners = [
        {
          acl = [ "pattern readwrite #" ];
          omitPasswordAuth = true;
          address = "127.0.0.1";
          settings.allow_anonymous = true;
        }
      ];
    };

    zigbee2mqtt = {
      enable = true;
      settings = {
        serial = {
          port = "/dev/serial/by-id/usb-Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0014D97058-if00";
          adapter = "zstack";
        };
        mqtt = {
          server = "mqtt://${(head config.services.mosquitto.listeners).address}:${toString (head config.services.mosquitto.listeners).port}";
        };
        frontend = {
          port = 8080;
          host = "127.0.0.1";
          url = "https://zigbee.nyanlout.re";
        };
        groups = {
          "101" = {
            friendly_name = "salon";
            devices = [
              "0x94deb8fffe760f3d"
            ];
          };
          "102" = {
            friendly_name = "cuisine";
            devices = [
              "0x003c84fffe6d9ee6"
            ];
          };
          "103" = {
            friendly_name = "entrée";
            devices = [
              "0x84ba20fffe5ec243"
            ];
          };
          "104" = {
            friendly_name = "tout";
            devices = [
              "0x94deb8fffe760f3d"
              "0x003c84fffe6d9ee6"
              "0x84ba20fffe5ec243"
            ];
          };
          "107" = {
            friendly_name = "chambre";
            devices = [
              "0x84ba20fffe5eb120"
            ];
          };
        };
      };
    };

    home-assistant = {
      enable = true;
      extraComponents = [
        # Components required to complete the onboarding
        "met"
        "radio_browser"
      ];
      config = {
        default_config = {};
        homeassistant = {
          country = "FR";
          latitude = 48.60038;
          longitude = 7.74063;
          elevation = 146;
        };
        meteo_france = null;
        http = {
          use_x_forwarded_for = true;
          trusted_proxies = [ "127.0.0.1" ];
        };
        mqtt = null;
        esphome = null;
        light = [
          {
            platform = "group";
            name = "Salon";
            entities = [
              "light.salon_light"
              "light.cuisine_light"
              "light.entree_light"
            ];
          }
        ];
        media_player = [
          {
            platform = "squeezebox";
            host = "10.30.0.1";
          }
        ];
      };
    };

    photoprism = {
      enable = true;
      originalsPath = "/mnt/backup_loutre/amandoleen/d/Users/Amand/Pictures";
      passwordFile = "/mnt/secrets/photoprism_pass";
      settings = {
        PHOTOPRISM_READONLY = "1";
        PHOTOPRISM_DETECT_NSFW = "1";
        PHOTOPRISM_SITE_URL = "https://photo.nyanlout.re/";
      };
    };
  };

  systemd = {
    timers."lg-devmode-reset" = {
      wantedBy = [ "timers.target" ];
        timerConfig = {
          OnBootSec = "5m";
          OnUnitActiveSec = "1w";
        };
    };
    services = {
      "borgbackup-job-loutre".serviceConfig.TemporaryFileSystem = ["/mnt/borgsnap"];
      "lg-devmode-reset" = {
        script = ''
          ${pkgs.curl}/bin/curl https://developer.lge.com/secure/ResetDevModeSession.dev\?sessionToken\=9f94269da0dc14fd924b65d8dca28b076f931ad1ca04fe7a09ac78cdb0e22cb4
        '';
        serviceConfig = {
          Type = "oneshot";
        };
      };
    };
  };

  dogetipbot-telegram.enable = true;

  ipmihddtemp.enable = true;

  users.groups.nginx.members = [ "matrix-synapse" ];

  security.pam.services.sshd.text = pkgs.lib.mkDefault( pkgs.lib.mkAfter "session optional ${pkgs.pam}/lib/security/pam_exec.so seteuid ${login_mail_alert}/bin/mail_alert.sh" );

  networking = {
    firewall.interfaces.eno2.allowedTCPPorts = [
      3260
    ];

    firewall.allowedTCPPorts = [
      20 21 # FTP
    ];

    firewall.allowedTCPPortRanges = [
      { from = 64000; to = 65535; } # FTP
    ];
  };
}
