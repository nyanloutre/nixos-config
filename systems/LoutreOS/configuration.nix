{ config, pkgs, inputs, ... }:

{
  imports = [
    ../common-cli.nix
    ./hardware-configuration.nix
    ./network.nix
    ./users.nix
    ./services.nix
  ];

  nix.settings.trusted-users = [ "root" "paul" ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    supportedFilesystems = [ "zfs" ];

    tmp.useTmpfs = true;

    # Enabling both boot.enableContainers & virtualisation.containers on system.stateVersion < 22.05 is unsupported
    enableContainers = false;
  };

  documentation.nixos.enable = false;

  services.zfs = {
    autoSnapshot.enable = true;
    autoScrub = {
      enable = true;
      interval = "monthly";
    };
  };

  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = "no";
      PasswordAuthentication = false;
      X11Forwarding = true;
    };
  };

  users = {
    groups.autossh = { };
    users.autossh = {
      home = "/home/autossh";
      createHome = true;
      group = "autossh";
      isSystemUser = true;
    };
  };

  virtualisation.podman.enable = true;

  security.sudo.wheelNeedsPassword = false;

  system.stateVersion = "18.03";
}
