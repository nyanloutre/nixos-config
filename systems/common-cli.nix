{ config, pkgs, ... }:

{

  time.timeZone = "Europe/Paris";

  environment.systemPackages = with pkgs; [
    # Editeurs
    (neovim.override {
      viAlias = true;
      vimAlias = true;
      configure = {
        customRC = ''
	        set tabstop=8
          set shiftwidth=4
          set softtabstop=0
          set expandtab
	        set smarttab
          set background=dark
          set mouse=
        '';
        packages.myVimPackage = with pkgs.vimPlugins; {
          start = [
            vim-startify  airline             sensible
            polyglot      ale                 fugitive
          ];
          opt = [ ];
        };
      };
    })

    # Gestionnaires de version
    tig
    gitAndTools.hub
    quilt

    # Gestion de paquets
    nix-prefetch-scripts
    nox
    nix-index

    # Système
    smartmontools
    htop
    lshw
    usbutils

    # Réseau
    inetutils
    rclone
    lftp
    nfs-utils
    nmap

    # Divers
    fzf
    file
    ncdu
    yt-dlp
    tldr
    starship

    # Audio
    beets

    # Outils
    borgbackup
    binutils
    bat
    molly-guard
    nix-template
    lz4

    # Développement
    openssl
  ];

  users.defaultUserShell = pkgs.zsh;
  programs = {
    tmux = {
      enable = true;
      clock24 = true;
    };

    zsh = {
      enable = true;
      autosuggestions.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      interactiveShellInit = ''
        source "$(${pkgs.fzf}/bin/fzf-share)/key-bindings.zsh"
        eval "$(starship init zsh)"
      '';
      ohMyZsh = {
        enable = true;
        plugins = [ "git" "colored-man-pages" "command-not-found" "extract" "nix" ];
        customPkgs = with pkgs;[
          nix-zsh-completions
        ];
      };
    };

    bash.interactiveShellInit = ''
      eval "$(starship init bash)"
    '';

    git.enable = true;
  };

  environment.variables = let
    starshipConfToml =
      pkgs.writeText "starship.toml" ''
        [[battery.display]]
        threshold = 50
      '';
  in {
    EDITOR = "nvim";
    STARSHIP_CONFIG = "${starshipConfToml}";
  };

  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 15d";
  systemd.timers.nix-gc.timerConfig.Persistent = true;

}
