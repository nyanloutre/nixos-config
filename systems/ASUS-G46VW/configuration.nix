# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ../common-cli.nix
      ../common-gui.nix
      ./hardware-configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.tmpOnTmpfs = true;
  boot.plymouth.enable = true;

  services.tlp.enable = true;
  services.tlp.extraConfig = ''
    START_CHARGE_THRESH_BAT0=85
    STOP_CHARGE_THRESH_BAT0=90
    CPU_SCALING_GOVERNOR_ON_BAT=powersave
    ENERGY_PERF_POLICY_ON_BAT=powersave
  '';

  networking.hostName = "rog-paul";

  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;

  networking.firewall.enable = false;

  networking.wireguard.interfaces = {
    wg0 = {
      ips = [ "192.168.20.2/24" ];
      allowedIPsAsRoutes = false;
      peers = [
        {
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "nyanlout.re:51820";
          publicKey = "NVLJ30kNP+fK+K3oM1LAcQpvZvz1dvBD8mWNjDf5Z2k=";
        }
      ];
      privateKeyFile = "/var/lib/wireguard/wg0.privatekey";
    };
  };

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];

  services.xserver.libinput.enable = true;
  services.xserver.libinput.naturalScrolling = true;

  users.extraUsers.paul = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "networkmanager" "wireshark" "dialout" ];
  };

  services.syncthing.enable = true;
  services.syncthing.user = "paul";
  services.syncthing.group = "users";

  services.redshift = {
    enable = true;
    temperature.night = 2700;
  };

  location.latitude = 48.573406;
  location.longitude = 7.752111;

  system.stateVersion = "18.03"; # Did you read the comment?
}
