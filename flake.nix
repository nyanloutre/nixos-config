{
  inputs = {
    nixpkgs.url = "flake:nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "flake:nixpkgs/nixos-unstable";
    # transmission 4.0.5 downgrade to fix tracker bug
    nixpkgs-4a3fc4cf7.url = "github:nixos/nixpkgs/4a3fc4cf736b7d2d288d7a8bf775ac8d4c0920b4";
    simple-nixos-mailserver = {
      url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-24.11";
      inputs = {
        nixpkgs.follows = "nixpkgs-unstable";
        nixpkgs-24_11.follows = "nixpkgs";
      };
    };
    dogetipbot-telegram = {
      url = "gitlab:nyanloutre/dogetipbot-telegram/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ipmihddtemp = {
      url = "gitlab:nyanloutre/ipmihddtemp/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    nixpkgs-unstable,
    nixpkgs-4a3fc4cf7,
    simple-nixos-mailserver,
    dogetipbot-telegram,
    ipmihddtemp
  }@inputs: {

    packages.x86_64-linux = (import ./pkgs nixpkgs.legacyPackages.x86_64-linux);

    nixosConfigurations.paul-fixe = nixpkgs-unstable.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        nixpkgs-unstable.nixosModules.notDetected
        {
          nixpkgs.config.allowUnfree = true;
          nix = {
            settings.experimental-features = [ "nix-command" "flakes" ];
            registry = {
              nixpkgs.to = {
                type = "path";
                path = nixpkgs-unstable.legacyPackages.x86_64-linux.path;
              };
            };
          };
        }
        ./systems/PC-Fixe/configuration.nix
      ];
    };

    nixosConfigurations.loutreos = nixpkgs.lib.nixosSystem rec {
      system = "x86_64-linux";
      specialArgs = {
        inputs = inputs;
        pkgs-4a3fc4cf7 = import nixpkgs-4a3fc4cf7 {
          inherit system;
        };
      };
      modules = [
        nixpkgs-unstable.nixosModules.notDetected
        simple-nixos-mailserver.nixosModule
        dogetipbot-telegram.nixosModule
        ipmihddtemp.nixosModule
        {
          nix = {
            settings.experimental-features = [ "nix-command" "flakes" ];
            registry = {
              nixpkgs.to = {
                type = "path";
                path = nixpkgs.legacyPackages.x86_64-linux.path;
              };
            };
          };
          systemd.services.watcharr = {
            description = "Watcharr";
            after = [ "network.target" ];
            environment = {
              PORT = "3005";
              WATCHARR_DATA = "/var/lib/watcharr";
            };
            serviceConfig = {
              DynamicUser = true;
              StateDirectory = "watcharr";
              ExecStart = "${self.packages.x86_64-linux.watcharr}/bin/Watcharr";
              PrivateTmp = true;
            };
            wantedBy = [ "multi-user.target" ];
          };
        }
        ./systems/LoutreOS/configuration.nix
      ];
    };

  };
}

